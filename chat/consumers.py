import asyncio 
import json
from django.contrib.auth import get_user_model
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from django.contrib.auth.models import User

from apis.models import Seller, Customer

from .models import Thread, ChatMessage

from rest_framework.authtoken.models import Token
from apis.views import sellerAuth, custAuth
from django.core import serializers
from asgiref.sync import async_to_sync, SyncToAsync



class CustomerNotification(AsyncConsumer):
    username = '';

    async def websocket_connect(self, event):
        await self.send({
            "type":"websocket.accept"
        })

    async def websocket_receive(self, event):
        data = event.get('text', None)
        req = json.loads(data)
        if req['req_type'] == 'auth':
            token = req['token'].split(" ")[1]
            username = req['username']

            customer = custAuth(username, token)
            if customer is not None:
                self.username = customer.user.username
                await self.channel_layer.group_add(str(username).split("@")[0], self.channel_name)
                chats = ChatMessage.objects.filter(thread__first= customer.user, read= False).exclude(user= customer.user).values_list('thread__second')

                res = {
                    "success":True,
                    "chats":len(set(chats)),
                    "type":"refresh"
                }

                await self.send({
                    "type":"websocket.send",
                    "text":json.dumps(res)
                })
            else:
                res = {
                    "success":False
                }
                await self.send({
                    "type":"websocket.disconnect"
                })

    
    async def websocket_disconnect(self, event):
        await self.channel_layer.group_discard(event["username"].split("@")[0], self.channel_name)
        
    
    async def chat_cnotif(self, event):
        print("called-------------------------")
        res = {
            "success":True,
            "seller": event['seller'],
            "chats": event['chats'],
            "type":"notification"
        }
        if event['sender'] != self.username:
            await self.send({
                "type":"websocket.send",
                "text":json.dumps(res)
            })
    



class CustomerChat(AsyncConsumer):
    username = None

    @database_sync_to_async
    def get_or_new(self, user, other_username): # get_or_create
        return Thread.objects.get_or_new(user, other_username)[0]

    async def websocket_connect(self, event):
        await self.send({
            "type":"websocket.accept"
        })

    async def websocket_receive(self, event):
        data = event.get('text', None)
        req = json.loads(data)
        #print(req)
        if req['req_type'] == 'auth':
            token = req['token'].split(" ")[1]
            username = req['username']
            

            customer = custAuth(username, token)

            if customer is not None:
                self.username = username
                await self.channel_layer.group_add(str(username).split("@")[0], self.channel_name)

                res = {
                'success':True,
                'no_chat':True,
                'chat':{},
                'sellers':[],
                'id':customer.user.id,
                'active_thread':{}
                }

                latest_msg = ChatMessage.objects.filter(thread__first= customer.user).order_by('-timestamp')

                if latest_msg.count() > 0:
                    msgs = ChatMessage.objects.filter(thread= latest_msg[0].thread)
                    msgs.update(read= True)
                    res['chat'] = serializers.serialize('json', msgs)
                    res['no_chat'] = False
                    res['active_thread'] = {
                        "other":latest_msg[0].thread.second.id,
                        "thread":latest_msg[0].thread.id
                    }

                sellers = Thread.objects.filter(first= customer.user).order_by('-timestamp').distinct()

                if sellers.count() > 0:
                    for s in sellers:
                        res['sellers'].append({
                            'id': s.second.id,
                            'name':s.second.first_name
                        }) 
                    
                await self.send({
                    "type":"websocket.send",
                    "text":json.dumps(res)
                })
            else:
                await self.send({
                    "type":"websocket.send",
                    "text":json.dumps({"success":False})
                })

        elif req['req_type'] == 'select':
            sel_id = req['selected_id']
            u_id = req['uid']

            if u_id == self.username:
                cust_user = User.objects.get(username= self.username)
                res = {
                'success':True,
                'no_chat':True,
                'chat':{},
                'sellers':[],
                'id':cust_user.id,
                'active_thread':{}
                }
                messages = ChatMessage.objects.filter(thread__first= cust_user, thread__second__id= sel_id)
                if messages.count() > 0:
                    res['no_chat'] = False
                    messages.update(read= True)
                    res['chat'] = serializers.serialize('json', messages)

                    sellers = Thread.objects.filter(first= cust_user).order_by('-timestamp').distinct()

                    if sellers.count() > 0:
                        for s in sellers:
                            res['sellers'].append({
                                'id': s.second.id,
                                'name':s.second.first_name
                            }) 
                    
                    res['active_thread'] = {
                        "other":messages[0].thread.second.id,
                        "thread":messages[0].thread.id
                    }

                    await self.send({
                        "type":"websocket.send",
                        "text":json.dumps(res)
                    })

            else:
                res = {
                    "success":False
                }
                await self.send({
                    "type":"websocket.send",
                    "text":json.dumps(res)
                })

        elif req['req_type'] == 'send_mesg':
            message = req['message']
            to= req['to']
            uid = req['uid']
            if uid == self.username:
                customer =User.objects.get(username= self.username)
                seller = User.objects.get(id = to)
                thread = Thread.objects.get(first= customer, second= seller)
                
                obj =   await SyncToAsync(ChatMessage.objects.create)(thread= thread, user= customer, message= message, read=False)
                

                if obj is not None:
                    res = {
                        'success':True,
                        'no_chat':False,
                        'chat':{},
                        'sellers':[],
                        'id':customer.id,
                        'active_thread':{}
                    }
                    messages = ChatMessage.objects.filter(thread__first= customer, thread__second__id= to)
                    res['chat'] = serializers.serialize('json', messages)

                    sellers = Thread.objects.filter(first= customer).order_by('-timestamp').distinct()

                    if sellers.count() > 0:
                        for s in sellers:
                            res['sellers'].append({
                                'id': s.second.id,
                                'name':s.second.first_name
                            }) 

                    res['active_thread'] = {
                        "other":messages[0].thread.second.id,
                        "thread":messages[0].thread.id
                    }
                    await self.send({
                        "type":"websocket.send",
                        "text":json.dumps(res)
                    })


    async def chat_cnotif(self, event):
        res = {
                'success':True,
                'no_chat':False,
                'chat':{},
                'sellers':[],
                'id':event['id'],
                'active_thread':{},
                "type":"new_mesg"
             }
        res['active_thread'] = {
            "other":event['thread_second'],
            "thread":event['thread']
        }

        msgs = ChatMessage.objects.filter(thread__first__id= event['id'], thread__second__id= event['thread_second'])
        res['chat'] = serializers.serialize('json', msgs)

        sellers = Thread.objects.filter(first__id= event['id']).order_by('-timestamp').distinct()

        if sellers.count() > 0:
            for s in sellers:
                res['sellers'].append({
                    'id': s.second.id,
                    'name':s.second.first_name
                }) 

        await self.send({
            "type":"websocket.send",
            "text":json.dumps(res)
        })
        






class SellerEndConsumer(AsyncConsumer):
    username= None

    @database_sync_to_async
    def get_or_new(self, user, other_username): # get_or_create
        return Thread.objects.get_or_new(user, other_username)[0]

    async def websocket_connect(self, event):
        print("conn - ", event)
        await self.send({
            "type":"websocket.accept"
        })
        
    async def websocket_receive(self, event):
        data = event.get('text', None)
        req = json.loads(data)
        print(req)
        if req['req_type'] == 'auth':
            token = req['token'].split(" ")[1]
            username = req['username']
            self.username = username

            seller = sellerAuth(username, token)
            
            if seller is not None:
                await self.channel_layer.group_add(str(username).split("@")[0], self.channel_name)
                res = {
                'success':True,
                'chat':{},
                'customers':[],
                'id':seller.user.id
                }

                msgs_obj = ChatMessage.objects.filter(thread__second__username= username)
                msgs = msgs_obj.order_by('-timestamp')
                res['chat'] = serializers.serialize('json', msgs)
                cust_list = []
                customers = msgs.values_list('thread__first__username', flat=True).distinct().order_by('-timestamp')
                msgs_obj.update(read=True)
                
                for c in customers:
                    if c not in cust_list:
                        cust_list.append(c)
                        print(cust_list)

                for c in cust_list:
                    obj = msgs.filter(thread__first__username= c)
                    res['customers'].append({
                        'id':obj[0].thread.first.id,
                        'name':obj[0].thread.first.first_name
                        })

                await self.send({
                    "type":"websocket.send",
                    "text": json.dumps(res)
                })
            else:
                await self.send({
                    "type":"websocket.disconnect",
                })
        elif req['req_type'] == 'send_mesg':
            if self.username is None:
                await self.send({
                    "type":"websocket.disconnect",
                })
            else:
                msg = req['message']
                seller =User.objects.get(username= self.username)
                cust = User.objects.get(id = req['to'])
                thread = Thread.objects.get(first= cust, second= seller)
                
                await SyncToAsync(ChatMessage.objects.create)(thread= thread, user= seller, message= msg)

    async def chat_notif(self, event):
        res = {
            'success':True,
            'chat':{},
            'customers':[],
            'id':None
            }

        msgs_obj = ChatMessage.objects.filter(thread__second__username= self.username)
        msgs = msgs_obj.order_by('-timestamp')
        res['id'] = msgs_obj[0].thread.second.id
        res['chat'] = serializers.serialize('json', msgs)
        cust_list = []
        customers = msgs.values_list('thread__first__username', flat=True).distinct().order_by('-timestamp')
        
        for c in customers:
            if c not in cust_list:
                cust_list.append(c)
                print(cust_list)

        for c in cust_list:
            obj = msgs.filter(thread__first__username= c)
            res['customers'].append({
                'id':obj[0].thread.first.id,
                'name':obj[0].thread.first.first_name
                })

        await self.send({
            "type":"websocket.send",
            "text": json.dumps(res)
        })

                        




class SellerChatConsumer(AsyncConsumer):
    token_obj = []
    username= ''

    async def websocket_connect(self, event):
        #print("conn - ", event)
        await self.send({
            "type":"websocket.accept"
        })


    async def websocket_receive(self, event):
        #print("receive", event)
        data = event.get('text', None)
        req = json.loads(data)
        if req['req_type'] == 'auth':
            token = req['token'].split(" ")[1]
            username = req['username']
            await self.channel_layer.group_add(str(username).split("@")[0], self.channel_name)

            try:
                token_obj = Token.objects.get(user__username= username, key= token)
                seller = Seller.objects.get(user= token_obj.user)
                self.token_obj = token_obj

                chats = ChatMessage.objects.filter(thread__second = token_obj.user, read= False).exclude(user= token_obj.user).values_list('thread__first')
                self.username = username

                res = {
                    "chats":  len(set(chats)),
                    "type":"refresh"
                }

                await self.send({
                    "type":"websocket.send",
                    "text": json.dumps(res)
                })

            except (Token.DoesNotExist, Seller.DoesNotExist, ChatMessage.DoesNotExist):
                await self.send({
                    "type":"websocket.disconnect",
                })

            #print(token)


        
    
    async def websocket_disconnect(self, event):
        await self.channel_layer.group_discard(event["username"].split("@")[0], self.channel_name)
        print("disconnected", event)

    async def chat_notif(self, event):
        chats = ChatMessage.objects.filter(thread__second__username = self.username, read= False).exclude(user__username= self.username).values_list('thread__first')


        if event['sender'] != self.username:
            res = {
                "customer": event['customer'],
                "chats":  len(set(chats)),
                "type": "notification"
            }
            await self.send({
                "type":"websocket.send",
                "text": json.dumps(res)
            })

class ChatConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        

        # me = self.scope['user']
        other_user = self.scope['url_route']['kwargs']['username']
        print("connected", event, other_user)
        # thread_obj = self.get_or_new(me, other_user)
        # self.thread_obj = thread_obj
        # print("thread is ", thread_obj.id)
        # chat_room = f"thread_{thread_obj.id}"
        # print("room", chat_room)
        # self.chat_room = chat_room

        # await self.channel_layer.group_add(
        #     chat_room,
        #     self.channel_name
        # )
        #await asyncio.sleep(30)
        # await self.send({
        #     "type":"websocket.send",
        #     "text":other_user
        # })
        await self.send({
            "type":"websocket.accept"
        })

    async def websocket_receive(self, event):
        print("receive", event)
        data = event.get('text', None)
        print(data)
        # if data is not None:
        #     dict_data = json.loads(data)
        #     msg = dict_data.get('message')
        #     print(msg)

        #     res = {
        #         'message': msg,
        #         'username':'kiran'
        #     }

        #     new_event = {
        #         "type":"websocket.send",
        #         "text": json.dumps(res)
        #     }

        #     self.create_chat(msg)

        #     await self.channel_layer.group_send(
        #         self.chat_room,
        #         {
        #             "type":"chat_message",
        #             "text":msg
        #         }
        #     )

    async def chat_message(self, event):
        await self.send({
            "type":"websocket.send",
            "text": event['text']
        })

    def create_chat(self, msg):
        me = self.scope['user']
        return ChatMessage.objects.create(thread = self.thread_obj, user = me, message = msg)

    async def websocket_disconnect(self, event):
        print("disconnected", event)


    #@database_sync_to_async
    def get_or_new(self, user, other_username): # get_or_create
        return Thread.objects.get_or_new(user, other_username)[0]