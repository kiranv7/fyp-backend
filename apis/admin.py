from django.contrib import admin
from .models import Product, Customer, DeliveryAddress, Order, Seller, AlternateSeller, Cart, SellerOTP, Review,  Banner

# Register your models here.
admin.site.site_header = 'WebCart Admin Panel'
admin.site.site_title = 'Admin Panel | WebCart'

class ProductAdmin(admin.ModelAdmin):
    search_fields = ('title','category',)
    list_display = ('id', 'title', 'seller', 'status', 'category', 'sub_category')

class OrderAdmin(admin.ModelAdmin):
    search_fields = ('seller__user__first_name','ordered_product__title')
    ordering= ('updated_on',)
    list_display = ('order_id','ordered_product', 'ordered_date', 'order_status','updated_on')

class AlternateSellerAdmin(admin.ModelAdmin):
    list_display = ('id', 'status','seller', 'stock')

admin.site.register(Product, ProductAdmin)
admin.site.register(Customer)
admin.site.register(DeliveryAddress)
admin.site.register(Order, OrderAdmin)
admin.site.register(Seller)
admin.site.register(AlternateSeller, AlternateSellerAdmin)
admin.site.register(Cart)
admin.site.register(SellerOTP)
admin.site.register(Review)
admin.site.register(Banner)

