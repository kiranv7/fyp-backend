from django.db import models
from django.contrib.auth.models import User
from autoslug import AutoSlugField
import math, random
import uuid, os
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _

# Create your models here.

PROD_CATEGORY_CHOICES = [
    ('Electronics', 'Electronics'),
    ('Men', 'Men'),
    ('Women', 'Women'),
    ('Home & Furniture', 'Home & Furniture'),
    ('Books', 'Books'),
]

order_statuses = [
    ('Ordered', 'Ordered'),
    ('Packed', 'Packed'),
    ('Shipped', 'Shipped'),
    ('Delivered', 'Delivered'),
    ('Cancelled', 'Cancelled'),
    ('Cancelled by seller', 'Cancelled by seller'),
    ('Return requested', 'Return requested')
]

seller_status = [
    ('approved', 'Approved'),
    ('pending', 'Pending'),
    ('rejected', 'Rejected')
]

product_status = [
    ('pending','Pending'),
    ('approved','Approved'),
    ('rejected', 'Rejected')
]

payment_types = [
    ('Online', 'Online'),
    ('COD', 'COD')
]

def prodimg_upload(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('product_images/', filename)

def bannerimg_upload(ins, fname):
    ext = fname.split('.')[-1]
    fname = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('banner_images/', fname)


class Seller(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    verified = models.BooleanField(default= False)
    phone = models.IntegerField()
    join_date = models.DateTimeField(auto_now_add= True)
    gstin = models.CharField(max_length= 30)
    company_name = models.CharField(max_length=40)
    bank_account_num = models.IntegerField()
    ifsc_code = models.CharField(max_length= 10)
    state = models.CharField(max_length= 50)
    city = models.CharField(max_length= 50)
    reason_for_rejection = models.TextField(max_length=400, default="", null= True, blank= True)
    status= models.CharField(max_length=50, choices= seller_status, default= 'pending')
    

    def __str__(self):
        return str(self.id) + " | " +str(self.user.username)

class AlternateSeller(models.Model):
    seller = models.ForeignKey(Seller, on_delete = models.CASCADE)
    discount_percentage = models.IntegerField(default=0)
    product_id= models.IntegerField()
    created_at = models.DateTimeField(auto_now_add= True)
    stock = models.IntegerField(default=1)
    reason_for_rejection = models.TextField(max_length=400, default="")
    status = models.CharField(choices= product_status, max_length=50, default= 'pending')

    def __str__(self):
        return str(self.product_id) + " | "+ self.seller.user.first_name

class Product(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=1500)
    category = models.CharField(max_length=300, choices= PROD_CATEGORY_CHOICES)
    sub_category =  models.CharField(max_length=300)
    image1 = models.FileField(null=True,blank=True,upload_to= prodimg_upload)
    price  = models.IntegerField(default=0)
    discount_percentage = models.IntegerField(default=0)
    seller = models.ForeignKey(Seller, on_delete = models.CASCADE)
    slug = AutoSlugField(populate_from='title')
    alternate_sellers = models.ManyToManyField(AlternateSeller, blank=True)
    status = models.CharField(max_length= 150, choices= product_status, default= 'pending')
    stock = models.IntegerField(default=1)

    def __str__(self):
        return str(self.id)+" | "+str(self.title)+" | "+str(self.category)


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    gender = models.CharField(max_length= 10)
    verified = models.BooleanField(default= False)
    phone = models.IntegerField()
    join_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.user.username) +' | '+str(self.user.id)


class DeliveryAddress(models.Model):
    customer = models.ForeignKey(Customer, on_delete = models.CASCADE)
    house_or_office_name = models.CharField(max_length= 60)
    pincode = models.IntegerField()
    landmark = models.CharField(max_length=100)
    district = models.CharField(max_length= 60)
    state = models.CharField(max_length= 60)

    def __str__(self):
        return str(self.customer.user.id) + " | " + str(self.customer.user.username)


refundStatus = (
    ('Not applied', 'Not applied'),
    ('Initiated', 'Initiated'),
    ('Success', 'Success'),
    ('Failed', 'Failed')
)


class Order(models.Model):
    ordered_product = models.ForeignKey(Product, on_delete = models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete = models.CASCADE)
    ordered_date = models.DateTimeField(auto_now_add= True)
    product_quantity = models.IntegerField()
    delivery_address = models.ForeignKey(DeliveryAddress, on_delete = models.CASCADE)
    order_status = models.CharField(max_length=100, choices= order_statuses)
    seller = models.ForeignKey(Seller, on_delete= models.CASCADE)
    expected_delivery = models.DateField(null= True, blank= True)
    order_id = models.CharField(max_length= 20)
    payment_type = models.CharField(max_length= 20, choices= payment_types)
    amount= models.DecimalField(max_digits=20, decimal_places=2)
    payment_success = models.BooleanField(default= False)
    transaction_id = models.CharField(max_length= 60, null=True, blank= True)
    refund_id = models.CharField(max_length= 60, null=True, blank= True)
    refund_ref_id = models.CharField(max_length= 60, null=True, blank= True)
    refund_status = models.CharField(max_length= 50, choices=refundStatus)
    updated_on = models.DateTimeField(auto_now= True)
    payed_to_admin= models.BooleanField(default=False)

    def __str__(self):
        return str(self.id) + " | "+str(self.ordered_product.title)

class Cart(models.Model):
    customer = models.ForeignKey(Customer, on_delete= models.CASCADE)
    product = models.ForeignKey(Product, on_delete= models.CASCADE)
    added_time = models.DateTimeField(auto_now_add=True)

class SellerOTP(models.Model):
    seller= models.ForeignKey(Seller, on_delete= models.CASCADE)
    otp = models.CharField(max_length=5, blank=True, null= True)
    created_at= models.DateTimeField(auto_now_add= True)

    def save(self, *args, **kwargs):
        self.otp = generateOTP()
        super(SellerOTP, self).save(*args, **kwargs)

class CustomerOTP(models.Model):
    customer = models.ForeignKey(Customer, on_delete= models.CASCADE)
    otp = models.CharField(max_length=5, blank= True, null= True)
    created_at= models.DateTimeField(auto_now_add= True)




class Review(models.Model):
    customer = models.ForeignKey(Customer, on_delete= models.CASCADE)
    rating = models.IntegerField()
    comment = models.TextField(max_length= 500, null= True, blank= True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    order = models.ForeignKey(Order, on_delete= models.CASCADE)


class Banner(models.Model):
    title = models.CharField(max_length= 60)
    image = models.FileField(null=True,blank=True,upload_to= bannerimg_upload)
    created_at = models.DateTimeField(auto_now_add= True)
    appear_till = models.DateTimeField()
    link = models.CharField(max_length= 60)
    updated_at = models.DateTimeField(auto_now= True)

    def __str__(self):
        return str(self.id) + ' | ' + self.title



@receiver(models.signals.post_delete, sender= Product)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `Product` object is deleted.
    """
    if instance.image1:
        if os.path.isfile(instance.image1.path):
            os.remove(instance.image1.path)

@receiver(models.signals.pre_save, sender= Product)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `Product` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = Product.objects.get(pk=instance.pk).image1
    except Product.DoesNotExist:
        return False

    new_file = instance.image1
    if not old_file == new_file:
        try:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
        except ValueError:
            return False

 
            
@receiver(models.signals.post_delete, sender= Banner)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `Product` object is deleted.
    """
    if instance.image:
        if os.path.isfile(instance.image.path):
            os.remove(instance.image.path)

@receiver(models.signals.pre_save, sender= Banner)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `Product` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = Banner.objects.get(pk=instance.pk).image
    except Banner.DoesNotExist:
        return False

    new_file = instance.image
    if not old_file == new_file:
        try:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)
        except ValueError:
            return False




def generateOTP(): 
    string = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    OTP = "" 
    length = len(string) 
    for i in range(4) : 
        OTP += string[math.floor(random.random() * length)] 
  
    return OTP

