from django.shortcuts import render
from django.contrib.auth import authenticate, login as dj_login, logout as dj_logout
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import ProductSerializer, LoginSerializer
from .models import Review, Banner, AlternateSeller, Product, Customer, Order, Seller, Cart, SellerOTP, CustomerOTP, PROD_CATEGORY_CHOICES, DeliveryAddress
from django.shortcuts import HttpResponse
from django.http import FileResponse
from rest_framework.decorators import api_view, permission_classes
import json
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
from django.core import serializers
import datetime, math, random
from django.db.models import Q, Sum, Avg
from django.core.mail import send_mail
from chat.models import Thread, ChatMessage
from decimal import Decimal
import pytz

class ProductViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = Product.objects.filter(status='approved') #.order_by('-date_joined')
    serializer_class = ProductSerializer
    lookup_field = 'slug'

class LoginView(APIView):
    permission_classes = ('')
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        dj_login(request, user)
        Token.objects.filter(user= user).delete()
        token= Token.objects.create(user= user)
        cart_count = Cart.objects.filter(customer__user= user).count()
        return Response({"success":True,"first_name":user.first_name,"username":user.username,"token":"Token "+token.key, "cart_count": cart_count}, status = 200)


class LogoutView(APIView):
    permission_classes = (AllowAny,)
    authentication_classes = (TokenAuthentication, )

    def post(self, request):
        dj_logout(request)
        return Response( {"success":True,"message":"Logged out successfully"} ,status = 200)


class SignupView(APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        if request.data['email'] and request.data['password']:
            email = request.data['email']
            password = request.data['password']
            try:
                user = User.objects.get(email = email)
                return Response({"success":False, "message":"User already exist with this email"})
            except User.DoesNotExist:
                userobj =  User.objects.create_user(
                    username= request.data['email'],
                    password = request.data['cpassword'],
                    email = request.data['email'],
                    first_name = request.data['firstName'],
                    last_name = request.data['lastName']
                )

                

                Customer.objects.create(
                    user = userobj,
                    gender = request.data['gender'],
                    phone = request.data['phone']
                )

                token= Token.objects.create(user= userobj)
                return Response({"success":True,"first_name":userobj.first_name,"username":userobj.username,"token":"Bearer "+token.key})



@api_view(['GET'])
def check(request):
    print(request.META.get(''))
    return Response('hello')

@api_view(['POST'])
@permission_classes((''))
def orders(request):
    username = request.data['username']
    try:
        user = Customer.objects.get(user__username = username)
        tokenreq = request.META.get('HTTP_AUTHORIZATION', '')
        try:
            token = Token.objects.get(user = user.user, key = tokenreq.split(' ')[1])
            # Token.objects.filter(user = user.user).delete()
            # newtoken = Token.objects.create(user= user.user)
            orders = Order.objects.filter(Q(customer= user, payment_type='COD') | Q(customer= user, payment_success= True, payment_type='Online'))

            msg = {
                "success":True,
                "token":"Bearer "+ token.key,
                "orders":[],
                "products":[],
            }

            for i in orders:
                msg["orders"].append(serializers.serialize('json', [i,]))
                msg["products"].append(serializers.serialize('json', [i.ordered_product, ]))

            return Response(msg)
        except Token.DoesNotExist:
            res = {
                "success":False,
                "message":"invalid token"
            }
            return Response(res)
    except Customer.DoesNotExist:
        res = {
            "success":False,
            "message":"Invalid user"
        }
        return Response(res)


@api_view(['POST'])
@permission_classes((''))
def checkToken(request):
    username = request.data['username']
    try:
        user = Customer.objects.get(user__username = username)
        tokenreq = request.META.get('HTTP_AUTHORIZATION', '')
        token = Token.objects.get(user = user.user, key = tokenreq.split(' ')[1])
        # Token.objects.filter(user = user.user).delete()
        # newtoken = Token.objects.create(user= user.user)
        min_time = datetime.datetime.now() - datetime.timedelta(7)
    
        if token.created.date() > min_time.date():
            msg = {
                "success":True
            }
            #print(token.created, min_time)
            return Response(msg)
        else:
            raise Exception
    except:
        msg = {
            "success":False
        }
        return Response(msg)



@api_view(['POST'])
@permission_classes((''))
def sellerLogin(request):
    sellerid = request.data['sellerid']
    password = request.data['password']

    user = authenticate(username= sellerid, password= password)

    if user is not None:
        try:
            seller = Seller.objects.get(user= user)
            Token.objects.filter(user= user).delete()
            token = Token.objects.create(user= user)
            res = {
                "success":True,
                "verified":seller.verified,
                "username": seller.user.username,
                "token":"Token "+token.key,
                "name":user.first_name,
                "status": seller.status
            }
            return HttpResponse(json.dumps(res))
        except Seller.DoesNotExist:
            res = {
                "success":False,
                "message":"invalid username or password"
            }
            return HttpResponse(json.dumps(res))

    else:
        res = {
            "success":False,
            "message":"invalid username or password"
        }
        return HttpResponse(json.dumps(res))


@api_view(['POST'])
@permission_classes((''))
def sellerDashboard(request):
    sellerid = request.data['sellerid']
    tokenreq = request.META.get('HTTP_AUTHORIZATION', '')

    token = tokenreq.split(' ')[1]

    try:
        seller = Seller.objects.get(user__username= sellerid)
        if seller.verified == False or seller.status in ['pending', 'rejected']:
            res = {
                "success":True,
                "verified": seller.verified,
                "seller_name":seller.company_name,
                "status": seller.status
            }
            return HttpResponse(json.dumps(res))
        try:
            tokenUser = Token.objects.get(user= seller.user)
            if token == tokenUser.key:
                products = Product.objects.filter(Q(seller= seller) | Q(alternate_sellers__seller= seller))
                orders = Order.objects.filter(seller= seller)

                succ_orders =  Order.objects.filter(seller= seller, order_status= "Delivered").count()
                pending_orders = Order.objects.filter(seller= seller).exclude(order_status= "Delivered").exclude(order_status= "Cancelled").count()

                rating = 3.45

                order_count = []
                for p in products:
                    order_count.append({
                        'product_id': p.id,
                        'orders': orders.filter(ordered_product= p, order_status= 'Delivered').count(),
                    })
                
                alt_products = []
                alt_obj = AlternateSeller.objects.filter(seller= seller)
                for a in alt_obj:
                    alt_products.append(
                        {
                           'product': serializers.serialize('json', [ getProductById(a.product_id), ]) ,
                           'info': serializers.serialize('json', [ a, ])
                        }
                    )

                
                

                res = {
                    "success":True,
                    "verified": seller.verified,
                    "seller_name":seller.company_name,
                    "total_products":products.count() + alt_obj.count(),
                    "status":seller.status,
                    "successful_orders":succ_orders,
                    "pending_orders":pending_orders,
                    "products":{},
                    "categories":{},
                    "rating":rating,
                    "order_count": order_count,
                    "alt_products": alt_products
                }

                
                res["products"] = (serializers.serialize('json', products))
                res["categories"] = json.dumps(dict(PROD_CATEGORY_CHOICES))

                return HttpResponse(json.dumps(res))
            else:
                raise Token.DoesNotExist
        except Token.DoesNotExist:
            res = {
                "success":False,
                "message":"invalid token"
            }
            return HttpResponse(json.dumps(res))
    except Seller.DoesNotExist:
        res = {
            "success":False,
            "message":"invalid seller"
        }
        return HttpResponse(json.dumps(res))

    tokenUser = Token.objects.get(key = token)
    return HttpResponse(token)


@api_view(['POST'])
@permission_classes((''))
def addToCart(request):
    token_req = request.META.get('HTTP_AUTHORIZATION', '')
    token_req = token_req.split(" ")[1]

    product_id = request.data['prod_id']
    username = request.data["username"]

    try:
        customer = Customer.objects.get(user__username= username)
        token = Token.objects.get(user= customer.user, key= token_req)

        min_time = datetime.datetime.now() - datetime.timedelta(5)

        if token.created.date() > min_time.date():
            product = Product.objects.get(pk= int(product_id))
            Cart.objects.get_or_create(product= product, customer= customer)

            cart_count = Cart.objects.filter(customer= customer).count()

            res = {
                "success":True,
                "cart_count": cart_count
            }
            return HttpResponse(json.dumps(res))
        else:
            raise Customer.DoesNotExist
    except Customer.DoesNotExist or Token.DoesNotExist:
        res= {
            "success":False,
            "message":"invalid token or account"
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def sellerRegister(request):
    email = request.data['email']
    seller_name = request.data['seller_name']
    company_name = request.data['company_name']
    phone = request.data['phone']
    password = request.data['password']
    gstin = request.data['gstin']
    acc_no = request.data['acc_no']
    state = request.data['your_state']
    city = request.data['your_city']
    ifsc = request.data['ifsc_code']

    others = Seller.objects.filter(Q(user__email= email) | Q(phone= phone) | Q(gstin= gstin) | Q(bank_account_num= acc_no) | Q(ifsc_code= ifsc)).count()
    if others > 0:
        res = {
            "success":False,
            "message":"User already exists with this credentials"
        }
        return HttpResponse(json.dumps(res))
    else:
        #sendSellerOTP(fname)
        if len(seller_name.split(" ")) > 1:
            user = User.objects.create_user(
                username= email,email= email, 
                first_name= seller_name.split(" ")[0], last_name= seller_name.split(" ")[1],
                password= password
            )
        else:
            user = User.objects.create_user(
                username= email,email= email, 
                first_name= seller_name.split(" ")[0], last_name= ' ',
                password= password
            )
            
        seller= Seller.objects.create(
            user= user,
            phone= phone,
            gstin= gstin,
            company_name= company_name,
            bank_account_num= acc_no,
            ifsc_code= ifsc,
            state= state,
            city= city
        )

        otp_obj = SellerOTP.objects.create(
            seller= seller
        )

        sendSellerOTP(otp_obj.otp, email)

        token= Token.objects.create(user= user)
        res={
            "success":True,
            "verified": seller.verified,
            "seller_name": company_name,
            "username": user.username,
            "token": "Token "+token.key,
            "status": seller.status

        }
        return HttpResponse(json.dumps(res))


@api_view(['POST'])
@permission_classes((''))
def sellerVerify(request):
    sellerid = request.data['username']
    otp = request.data['otp']
    tokenreq = request.META.get('HTTP_AUTHORIZATION', '')

    token = tokenreq.split(' ')[1]
    try:
        seller = Seller.objects.get(user__username= sellerid)
        token = Token.objects.get(user= seller.user, key= token).key
        real_otp = SellerOTP.objects.get(seller= seller)
        r_otp = real_otp.otp
        

        if r_otp == otp:
            seller.verified = True
            seller.save()
            res = {
                "success":True,
                "status": seller.status
            }
            send_mail('Seller Account Verified', 'Your seller registration OTP verified successfuly! Your registration is under review, you will get review result as email', 'shopio.project@gmail.com', [seller.user.email], fail_silently= False)
            return HttpResponse(json.dumps(res))
        else:
            res = {
                "success":False,
                "message":"Incorrect OTP."
            }
            return HttpResponse(json.dumps(res))
    except (SellerOTP.DoesNotExist, Token.DoesNotExist):
        res={
            "success":False,
            "message": "Invalid auth token or seller."
        }
        return HttpResponse(json.dumps(res))

    return HttpResponse()


@api_view(['POST'])
@permission_classes((''))
def resend_sellerOtp(request):
    sellerid = request.data['username']
    tokenreq = request.META.get('HTTP_AUTHORIZATION', '')

    token = tokenreq.split(' ')[1]

    try:
        seller = Seller.objects.get(user__username= sellerid)
        Token.objects.get(user= seller.user, key= token)
        SellerOTP.objects.filter(seller= seller).delete()

        otp_obj = SellerOTP.objects.create(
            seller= seller
        )
        sendSellerOTP(otp_obj.otp, seller.user.email)
        res= {
            "success":True
        }

        return HttpResponse(json.dumps(res))
    except (Seller.DoesNotExist, Token.DoesNotExist):
        res = {
            "success":False,
            "message":"invalid seller or auth token"
        }

        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def addProduct(request):
    title = request.data['title']
    desc = request.data['description']
    category = request.data['category']
    subcategory = request.data['sub_category']
    price = request.data['price']
    disc = request.data['discount_percentage']
    image = request.data['image']
    username = request.data['username']
    stock = request.data['stock']
    token = request.data['token'].split(" ")[1]

    try:
        seller = Seller.objects.get(user__username= username)
        token_obj = Token.objects.get(user= seller.user, key = token)

        if seller.status == "approved":
            Product.objects.create(
                title= title,
                description= desc,
                category= category,
                sub_category= subcategory,
                price= price,
                discount_percentage= disc,
                image1= image,
                seller= seller,
                stock= stock
            )
            res = {
                "success":True
            }
            return HttpResponse(json.dumps(res))
        else:
            raise Seller.DoesNotExist

    except (Seller.DoesNotExist, Token.DoesNotExist):
        res = {
            "success":False,
            "message":"Invalid seller or auth token"
        }
        return HttpResponse(json.dumps(res))


@api_view(['POST'])
@permission_classes((''))
def deleteProduct(request):
    username = request.data['username']
    slug = request.data['slug']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(" ")[1]

    try:
        seller = Seller.objects.get(user__username= username)
        token_obj = Token.objects.get(user= seller.user, key = token)

        
        seller_type = request.data['type']

        if seller_type == 'alt':
            AlternateSeller.objects.filter(id= slug).delete()
            res = {
                "success":True,
                "message":"You are no more selling this product"
            }
            return HttpResponse(json.dumps(res))
        
        product = Product.objects.get(slug= slug)
        if product.seller == seller:
            alt_sellers = product.alternate_sellers
            if alt_sellers.count() > 0:
                #alternate seller becomes the main seller
                new_seller = product.alternate_sellers.all().order_by('created_at')[0]
                product.seller = new_seller.seller
                product.save()
                new_seller.delete()
            else:
                product.delete()

            res = {
                "success":True,
                "message":product.title + " deleted successfuly.",
            }
            return HttpResponse(json.dumps(res))

        elif product.alternate_sellers.filter(seller= seller).count() == 1:
            product.alternate_sellers.get(seller= seller).delete()
            res = {
                "success":True,
                "message":product.title + " deleted successfuly.",
            }
            return HttpResponse(json.dumps(res))
        else:
            raise Seller.DoesNotExist

    except (Seller.DoesNotExist, Token.DoesNotExist, AlternateSeller.DoesNotExist, Product.DoesNotExist):
        res = {
            "success":False,
            "message":"unable to delete the product."
        }
        return HttpResponse(json.dumps(res))


@api_view(['POST'])
@permission_classes((''))
def getSeller(request):
    id = request.data['id']
    try:
        seller = Seller.objects.get(id = id)
        prod1 = Product.objects.filter(status='approved', stock__gte=1, seller= seller).count()
        prod2 = AlternateSeller.objects.filter(status='approved', stock__gte=1, seller= seller).count()
        orders = Order.objects.filter(seller= seller, order_status= 'Delivered').count()

        res = {
            "success":True,
            "first_name":seller.user.first_name,
            "last_name":seller.user.last_name,
            "total_products":(prod1 + prod2),
            "successful_orders": orders,
            "state":seller.state,
            "email":seller.user.email,
            "id":seller.id
        }
    except Seller.DoesNotExist:
        res = {
            "success":False
        }
    return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def startChat(request):
    token = request.META.get('HTTP_AUTHORIZATION', '').split(" ")[1]
    to_id= request.data['to']
    from_id = request.data['from'] 
    message = request.data['message']

    customer = custAuth(from_id, token)
    try:
        seller = Seller.objects.get(id= to_id)
    except Seller.DoesNotExist:
        res= {
            "success":False,
            "message":"Unable to send message to this seller."
        }

    if customer is not None:
        try:
            thread_obj = Thread.objects.get(first= customer.user, second= seller.user)
            res = {
                "success":True,
                "thread_exist":True
            }
            msg = ChatMessage.objects.create(thread= thread_obj, user= customer.user, message= message)
        
        except Thread.DoesNotExist:
            thread_obj = Thread.objects.create(first= customer.user, second= seller.user) 
            msg = ChatMessage.objects.create(thread= thread_obj, user= customer.user, message= message)
            res = {
                "success":True,
                "thread_exist":False
            }
        return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))


@api_view(['POST'])
@permission_classes((''))
def getCustomer(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)

    if customer is not None:
        orders = Order.objects.filter(customer= customer)
        addresses = DeliveryAddress.objects.filter(customer= customer)

        res = {
            "success":True,
            "profile":{
                "first_name":customer.user.first_name,
                "last_name":customer.user.last_name,
                "gender": customer.gender,
                "phone": customer.phone,
                "email":customer.user.email,
            },
            "delivery_addresses":[],
            "total_orders": orders.count()

        }
        res["delivery_addresses"] = serializers.serialize('json', addresses)
        return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed."
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def updateProfile(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)
    if customer is not None:
        data = request.data['data']
        email = data['email']
        first_name = data['firstName']
        last_name = data['lastName']
        phone = data['phone']
        gender = data['gender']

        checkemail = User.objects.filter(email= email).exclude(id= customer.user.id)
        checkphone = Customer.objects.filter(phone= phone).exclude(id= customer.id)
        if checkemail.count() > 0 or checkphone.count() > 0:
            res = {
                "success":False,
                "message":"email id or phone number already in use"
            }
            return HttpResponse(json.dumps(res))
        else:
            customer.user.email = email
            customer.user.username = email
            customer.user.first_name = first_name
            customer.user.last_name = last_name
            customer.phone = phone
            customer.gender = gender

            customer.save()
            customer.user.save()
            res= {
                "success":True,
                "message":"profile updated successfuly"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def addAddress(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)
    if customer is not None:
        data = request.data['data']
        house_name = data['house_or_office_name']
        state = data['state']
        landmark = data['landmark']
        pincode = data['pincode']
        district = data['district']

        DeliveryAddress.objects.create(
            customer= customer,
            house_or_office_name= house_name,
            pincode= pincode,
            state= state,
            district= district,
            landmark= landmark
        )
        addrs = DeliveryAddress.objects.filter(
            customer= customer
        )
        res = {
            "success":True,
            "message":"delivery address added successfuly",
            "addresses":serializers.serialize('json', addrs)
        }
        return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def deleteAddress(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)
    if customer is not None:
        a_id = request.data['id']
        try:
            DeliveryAddress.objects.filter(customer= customer, id= a_id).delete()
            addresses = DeliveryAddress.objects.filter(customer= customer)
            res = {
                "success":True,
                "message":"address deleted successfuly",
                "addresses": serializers.serialize('json', addresses)
            }
            return HttpResponse(json.dumps(res))
        except (DeliveryAddress.DoesNotExist):
            res = {
                "success":False,
                "message":"unable to delete this address"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))
        


@api_view(['POST'])
@permission_classes((''))
def editAddress(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)
    if customer is not None:
        a_id = request.data['id']
        data = request.data['data']
        try:
            addr = DeliveryAddress.objects.get(customer= customer, id= a_id)
            addr.house_or_office_name = data['house_or_office_name']
            addr.pincode = data['pincode']
            addr.state = data['state']
            addr.district = data['district']
            addr.landmark = data['landmark']
            addr.save()

            address = DeliveryAddress.objects.filter(
                customer= customer
            )
            res={
                "success":True,
                "message":"address updated successfuly",
                "addresses":serializers.serialize('json', address)
            }
            return HttpResponse(json.dumps(res))
        except DeliveryAddress.DoesNotExist:
            res = {
                "success":False,
                "message":"unable to update this address"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def checkoutData(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    customer = custAuth(username, token)
    if customer is not None:
        slug = request.data['product']
        sellerid = request.data['seller']
        try:
            product = Product.objects.get(slug= slug)
            price = product.price
            addresses = DeliveryAddress.objects.filter(customer= customer)

            if product.seller.id == sellerid:
                discount = product.discount_percentage
                final_price = price - price * (discount / 100)
                stock = product.stock

                res = {
                    "success":True,
                    "final_price":final_price,
                    "stock":stock,
                    "product":serializers.serialize('json', [ product, ]),
                    "addresses": serializers.serialize('json', addresses)
                }
                return HttpResponse(json.dumps(res))

            else:
                altseller = product.alternate_sellers.get(seller__id= sellerid)
                discount = altseller.discount_percentage
                final_price = price - price * (discount / 100)
                stock = altseller.stock
                addresses = DeliveryAddress.objects.filter(customer= customer)

                res = {
                    "success":True,
                    "final_price":final_price,
                    "stock":stock,
                    "product":serializers.serialize('json', [ product, ]),
                    "addresses": serializers.serialize('json', addresses)

                }
                return HttpResponse(json.dumps(res))

        except (Product.DoesNotExist, AlternateSeller.DoesNotExist):
            res = {
                "success":False,
                "message":"Unable to fetch product info"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))


import requests
from django.conf import settings
from . import Checksum

@api_view(['POST', 'GET'])
@permission_classes((''))
def payment(request):
    MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
    MERCHANT_ID = settings.PAYTM_MERCHANT_ID

    username = request.POST['username']
    token = str(request.POST['token']).split(" ")[1]

    customer = custAuth(username, token)

    if customer is not None:
        cust_id = customer.id
        data= json.loads(request.data['data'])
        amount= data.get('amount', None)
        prod_id = data['product_id']
        address = data['address']
        qty = data['quantity']
        seller_id = data['seller']
        payment_choosen = data['payment']


        try:
            product = Product.objects.get(id= prod_id)
            address = DeliveryAddress.objects.get(id= address)
            
            if product.seller.id == seller_id:
                seller = product.seller
            else:
                seller_obj = product.alternate_sellers.get(id= seller_id)
                seller = seller_obj.seller

            order_id = Checksum.__id_generator__()

            if payment_choosen == 'Online':
                CALLBACK_URL = settings.HOST_URL + settings.PAYTM_CALLBACK_URL + str(cust_id) + '/'
                

                if amount:
                    data_dict = {
                        'MID': MERCHANT_ID,
                        'ORDER_ID': order_id,
                        'TXN_AMOUNT': amount,
                        'CUST_ID': customer.user.email,
                        'INDUSTRY_TYPE_ID': 'Retail',
                        'WEBSITE': settings.PAYTM_WEBSITE,
                        'CHANNEL_ID': 'WEB',
                        'CALLBACK_URL': CALLBACK_URL,
                    }
                    param_dict = data_dict
                    param_dict['CHECKSUMHASH'] = Checksum.generate_checksum(data_dict, MERCHANT_KEY)
                    url = 'https://securegw-stage.paytm.in/theia/processTransaction'

                    x = requests.post(url, data = param_dict)

                    order = Order.objects.create(
                        ordered_product= product,
                        customer= customer,
                        product_quantity= qty,
                        delivery_address= address,
                        order_status= 'Ordered',
                        seller= seller,
                        order_id= order_id,
                        payment_type= 'Online',
                        amount= amount
                    )

                    return HttpResponse(x)
            elif payment_choosen == 'COD':
                order = Order.objects.create(
                    ordered_product= product,
                    customer= customer,
                    product_quantity= qty,
                    delivery_address= address,
                    order_status= 'Ordered',
                    seller= seller,
                    order_id= order_id,
                    payment_type= 'COD',
                    amount= amount
                )

                context = {
                    "success":True
                }

                return render(request, "order_confirmation.html", context)

        except (
                Product.DoesNotExist, 
                DeliveryAddress.DoesNotExist,
                AlternateSeller.DoesNotExist
            ):
            res = {
                "success":False,
                "message":"unable make order"
            }
            return HttpResponse(json.dumps(res))
            
    else:
        res = {
            "success":False,
            "message":"authentication failed"
        }
        return HttpResponse(json.dumps(res))
  
    
@api_view(['POST', 'GET'])
@permission_classes((''))
def response(request, cust_id):
    if request.method == "POST":
        MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY
        data_dict = {}
        print('payment response', request.data)
        for key in request.POST:
            data_dict[key] = request.POST[key]
        if data_dict.get('CHECKSUMHASH', False) and data_dict['STATUS'] != 'TXN_FAILURE':
            verify = Checksum.verify_checksum(data_dict, MERCHANT_KEY, data_dict['CHECKSUMHASH'])
        else:
            verify = False
        if verify:
            for key in request.POST:
                if key == "BANKTXNID" or key == "RESPCODE":
                    if request.POST[key]:
                        data_dict[key] = int(request.POST[key])
                    else:
                        data_dict[key] = 0
                elif key == "TXNAMOUNT":
                    data_dict[key] = float(request.POST[key])
                
            print(data_dict)
            # REPLACE USERNAME WITH PRIMARY KEY OF YOUR USER MODEL
            #PaytmHistory.objects.create(user=User.objects.get(username=user_id), **data_dict)
            Order.objects.filter(order_id = data_dict['ORDERID'], customer__id = cust_id).update(payment_success= True, transaction_id= data_dict['TXNID'])
            context = {
                "success":True
            }
            return render(request, "order_confirmation.html", context)
        else:
            Order.objects.filter(order_id = data_dict['ORDERID'], customer__id = cust_id).update(payment_success= False)
            context = {
                "success":False
            }
            return render(request, "order_confirmation.html", context)
    else:
        return HttpResponse("Method \"GET\" not allowed")

    return HttpResponse(status=200)




@api_view(['POST'])
@permission_classes((''))
def getOrder(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]
    
    customer = custAuth(username, token)
    if customer is not None:
        orderid = request.data['orderid']
        try:
            order = Order.objects.get(order_id= orderid)
            res = {
                "success":True,
                "order": serializers.serialize('json', [ order, ]),
                "product": serializers.serialize('json', [ order.ordered_product, ]),
                "address": serializers.serialize('json', [ order.delivery_address, ]),
                "seller_name":order.seller.user.first_name
            }
            return HttpResponse(json.dumps(res))
        except (Order.DoesNotExist):
            res = {
                "success": False,
                "message": "order does not exist."
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success": False,
            "message": "Authentication failed."
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def cancelOrder(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]
    
    customer = custAuth(username, token)
    if customer is not None:
        #return HttpResponse('succs')
        order_id = request.data['order']
        try:
            order = Order.objects.get(order_id= order_id)
            if order.payment_type == 'Online' and order.payment_success == True:
                MERCHANT_ID = settings.PAYTM_MERCHANT_ID
                MERCHANT_KEY = settings.PAYTM_MERCHANT_KEY

                refId= Checksum.__id_generator__()
                param_dict = dict()

                param_dict["body"] = {
                    "mid":MERCHANT_ID,
                    "orderId": order.order_id,
                    "refId": refId,
                    "txnId":order.transaction_id,
                    "refundAmount": str(order.amount),
                    "txnType":"REFUND"
                }

                checksum = Checksum.generate_checksum_by_str(json.dumps(param_dict["body"]), MERCHANT_KEY)

                param_dict["head"] = {
                    "clientId" : "C11",
                    "signature": checksum
                }

                Order.objects.filter(order_id= order_id).update(
                    refund_ref_id= refId
                )
                
                url = "https://securegw-stage.paytm.in/refund/apply"
                # print(param_dict)
                response = requests.post(url, data = json.dumps(param_dict), headers = {"Content-type": "application/json"}).json()

                #print(x.text)
                print(response)
                if response['body']['resultInfo']['resultCode'] == '601':
                    Order.objects.filter(order_id = response['body']['orderId']).update(refund_status= 'Initiated', refund_id= response['body']['refundId'], order_status= 'Cancelled')
                    
                    order = Order.objects.get(order_id= response['body']['orderId'])
                    res = {
                        "success":True,
                        "message":"Refund initiated successfully! Amount will be credited within few hours.",
                        "order": serializers.serialize('json', [ order, ])
                    }
                    return HttpResponse(json.dumps(res))
                else:
                    res = {
                        "success":False,
                        "message": "Some error occured. please try again"
                    }
                    return HttpResponse(json.dumps(res))
                return HttpResponse(response)

            elif order.payment_type == 'COD' :
                order.order_status = 'Cancelled'
                order.save()

                res = {
                    "success":True,
                    "message":"Order cancelled successfuly."
                }
                return HttpResponse(json.dumps(res))
            
        except Order.DoesNotExist:
            res = {
                "success": False,
                "message": "Invalid order"
            }
            return HttpResponse(json.dumps(res))
            
    else:
        res = {
            "success": False,
            "message": "Authentication failed."
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def checkRefundStatus(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]
    
    customer = custAuth(username, token)
    if customer is not None:
        orderId = request.data['order']
        order = Order.objects.get(order_id = orderId)
        if order.refund_id is not None:
            param_dict = {}
            param_dict['body'] = {
                'mid': settings.PAYTM_MERCHANT_ID,
                'orderId': order.order_id,
                'refId': order.refund_ref_id
            }

            checksum = Checksum.generate_checksum_by_str(json.dumps(param_dict["body"]), settings.PAYTM_MERCHANT_KEY)

            param_dict["head"] = {
                "clientId" : "C11",
                "signature": checksum
            }
            post_data = json.dumps(param_dict)
            url = "https://securegw-stage.paytm.in/v2/refund/status"

            response = requests.post(url, data = post_data, headers = {"Content-type": "application/json"}).json()
            print('status res', response)
            
            if response['body']['resultInfo']['resultCode'] == '10':
                Order.objects.filter(order_id= orderId).update(refund_status= 'Success', expected_delivery= None)
                order_obj = Order.objects.get(order_id = orderId)
                res = {
                    "success":True,
                    "order": serializers.serialize('json', [ order_obj, ])
                }
                return HttpResponse(json.dumps(res))
            else:
                res = {
                    "success":False,
                    "message":response['body']['resultInfo']['resultCode']
                }
                return HttpResponse(json.dumps(res))

    else:
        res = {
            "success": False,
            "message": "Authentication failed."
        }
        return HttpResponse(json.dumps(res))





@api_view(['POST'])
@permission_classes((''))
def feedback(request):
    username = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]
    
    customer = custAuth(username, token)
    if customer is not None:
        comment = request.data['comment']
        rating = request.data['rating']
        order_id = request.data['order']
        order = Order.objects.get(order_id= order_id)

        try:
            if order.order_status == 'Delivered':
                review = Review.objects.get(order= order)
                if comment == '':
                    review.comment = None
                else:
                    review.comment = comment
                review.rating = int(rating)
                review.save()

                res = {
                    "success": True,
                    "message": "Your feedback updated successfuly"
                }
                return HttpResponse(json.dumps(res))
            else:
                res = {
                    "success": False,
                    "message": "You can't give feedback to this product"
                }
                return HttpResponse(json.dumps(res))
        except Review.DoesNotExist:
            Review.objects.create(
                customer= customer,
                rating= int(rating),
                comment= comment,
                order= order
            )
            res = {
                "success": True,
                "message":  "Thanks, Your feedback is submitted successfuly"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success": False,
            "message": "Authentication failed."
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def editProduct(request):
    title = request.data['title']
    desc = request.data['description']
    category = request.data['category']
    subcategory = request.data['sub_category']
    price = request.data['price']
    disc = request.data['discount_percentage']
    image = request.data['image']
    username = request.data['username']
    stock = request.data['stock']

    product_id = request.data['product']

    sellerid = request.data['username']
    token = request.data['token'].split(' ')[1]

    seller = sellerAuth(sellerid, token)
    print('seller is', seller)
    if seller is not None:
        try:
            product = Product.objects.get(id= product_id)
            
            if product.seller == seller:
                product.title = title
                product.description = desc
                product.category = category
                product.sub_category = subcategory
                product.price = price
                product.discount_percentage = disc
                if image != 'null':
                    product.image1 = image 
                product.stock = stock
                product.save()

                res = {
                    "success": True,
                    "message":"Product edited successfuly"
                }

                return HttpResponse(json.dumps(res))
            else:
                res = {
                    "success":False,
                    "message":"You can't edit this product"
                }
                return HttpResponse(json.dumps(res))
        except (Product.DoesNotExist):
            res = {
                "success":False,
                "message":"Product does not exist."
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed."
        }
        return HttpResponse(json.dumps(res))


        


@api_view(['POST'])
@permission_classes((''))
def pendingOrders(request):
    username = request.data['username']
    token =  token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(username, token)
    
    if seller is not None:
        orders_list = Order.objects.filter(seller= seller).exclude(
            order_status= 'Delivered'
        ).exclude(
            order_status= 'Cancelled'
        )

        expdel_notset = orders_list.filter(expected_delivery = None)
        products = []
        addresses = []

        for o in orders_list:
            products.append(o.ordered_product)
            addresses.append(o.delivery_address)

        

        if orders_list.count() > 0:
            res = {
                "success":True,
                "pending_orders":True,
                "orders": serializers.serialize('json', orders_list),
                "products": serializers.serialize('json', products),
                "addresses": serializers.serialize('json', addresses),
                "expdel_notset": expdel_notset.count()
            }
            return HttpResponse(json.dumps(res))
        else:
            res = {
                "success":True,
                "pending_orders":False
            }
            return HttpResponse(json.dumps(res))
    else:
        res  = {
            "success":False,
            "message":"authentication failed."
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def setOrderStatus(request):
    username = request.data['username']
    token =  token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(username, token)
    if seller is not None:
        day = request.data['day']
        month = request.data['month']
        year = request.data['year']

        status = request.data['status']
        orderid = request.data['order_id']
        try:
            order = Order.objects.get(order_id= orderid)
            date = datetime.date(year, month, day)

            order_status =  'Cancelled by seller' if status == 'Cancelled' else status 

            print(order_status)

            order.expected_delivery = date
            order.order_status = order_status
            order.save()

            res = {
                "success":True,
                "message": "order updated successfuly."
            }
            return HttpResponse(json.dumps(res))
        except (Order.DoesNotExist):
            res = {
                "success":False,
                "message":"unable to fetch order"
            }
            return HttpResponse(json.dumps(res))
    else:
        res  = {
            "success":False,
            "message":"authentication failed."
        }
        return HttpResponse(json.dumps(res))
        



@api_view(['GET'])
@permission_classes((''))
def banners(request):
    utc = pytz.UTC

    now = datetime.datetime.now()
    out_banners = []

    banners = Banner.objects.all()

    for banner in banners:
        time = utc.localize(banner.appear_till.replace(tzinfo= None))
        now = utc.localize(now)
        
        print(time, now)

        if now <= time:
            out_banners.append(banner)
    res = {
        "success":True,
        "banners": serializers.serialize('json', out_banners)
    }
    return HttpResponse(json.dumps(res))







@api_view(['POST'])
@permission_classes((''))
def getSellerEarnings(request):

    sellerid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(sellerid, token) 

    if seller is not None:
        seven_days = datetime.date.today() + datetime.timedelta(days= 7)
        total = 0
        orders = Order.objects.filter(seller= seller, order_status='Delivered', payed_to_admin=False)
        
        for o in orders:
            if o.expected_delivery is not None:
                diff = datetime.date.today() - o.expected_delivery 
                if diff.days >= 7:
                    total += o.amount

        earnings = 0

        if total > 0 :
            earnings = Decimal((8.5 / 10)) * total
    
        res = {
            "success": True,
            "earnings": float(earnings)
        }
        return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed."
        }
        return HttpResponse(json.dumps(res))





from django.http import HttpResponsePermanentRedirect, HttpResponseNotFound, HttpResponseBadRequest, HttpResponse
from django.core.files.storage import default_storage
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.exceptions import InvalidImageFormatError
import re


SIZE_RE = re.compile(r'^(\d+),(\d+)$')

@api_view(['GET'])
@permission_classes((''))
def resize(request):
    thumbnail_opts = {}
    path = request.GET.get('path', '')
    size = request.GET.get('size', None)
    print('size is', size)
    if size is not None:
        if SIZE_RE.match(size):
            thumbnail_opts['size'] = tuple(size.split(','))
            if 'crop' in request.GET:
                thumbnail_opts['crop'] = request.GET['crop']
        else:
            return HttpResponse('no size specified')
    else:
        return HttpResponsePermanentRedirect(default_storage.url(path))
    try:
        thumbnailer = get_thumbnailer(default_storage, path)
        thumbnail = thumbnailer.get_thumbnail(thumbnail_opts)
        return HttpResponsePermanentRedirect(thumbnail.url)
    except IOError:
        return HttpResponseNotFound(u'File not found.')
    except InvalidImageFormatError:
        return HttpResponseBadRequest(u'File is not an image.')







@api_view(['GET'])
@permission_classes((''))
def searchProduct(request):
    inp = request.GET['input']
    ratings = []
    results = Product.objects.filter(status='approved').filter(Q(title__icontains=inp) | Q(category__icontains=inp) | Q(description__icontains= inp) | Q(sub_category__icontains=inp))
    for r in results:
        rating = Review.objects.filter(order__ordered_product= r).aggregate(Avg('rating'))
        if rating['rating__avg'] is not None:
            ratings.append({
                'product_id':r.id,
                'rating':rating['rating__avg']
            })
    
    res = {
        "success":True,
        "result": serializers.serialize('json', results),
        "ratings":ratings
    }
    return HttpResponse(json.dumps(res))






#for both seller and  customer
@api_view(['POST'])
@permission_classes((''))
def getReviews(request):
    product = request.data['product']
    reviews = Review.objects.filter(order__ordered_product__slug= product)
    customers = []

    for r in reviews:
        customers.append({'id':r.customer.id, 'first_name':r.customer.user.first_name})

    res = {
        "success":True,
        "reviews":serializers.serialize('json', reviews),
        "customers":customers
    }
    return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def altSellerRequest(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(userid, token)
    if seller is not None:
        selling_price = request.data['selling_price']
        product = request.data['product']

        try:
            product_obj = Product.objects.get(slug= product)
            try:
                altseller = AlternateSeller.objects.get(seller= seller, product_id= product_obj.id)
                res = {
                    "success":True,
                    "message":"You are already selling this product"
                }
                return HttpResponse(json.dumps(res))
            except AlternateSeller.DoesNotExist:
                stock = request.data['stock']
                discount = 100 - ( ( int(selling_price) / product_obj.price ) * 100 )

                AlternateSeller.objects.create(
                    seller= seller,
                    product_id= product_obj.id,
                    stock= stock,
                    discount_percentage= discount
                )

                res = {
                    "success": True,
                    "message": "Your selling request is sent!"
                }
                return HttpResponse(json.dumps(res))
        except Product.DoesNotExist:
            res = {
                "success":False,
                "message": "Unable to find the product"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed"
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def altInfo(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(userid, token)
    if seller is not None:
        alt_id = request.data['id']
        try:
            alt_obj = AlternateSeller.objects.get(id = int(alt_id))
            res = {
                "success":True,
                "info": serializers.serialize('json', [ alt_obj, ])
            }
            return HttpResponse(json.dumps(res))
        except AlternateSeller.DoesNotExist:
            res = {
                "success":False,
                "message":"couldn't fetch info"
            }
            return HttpResponse(json.dumps(res))

    else:
        res = {
            "success":False,
            "message":"Authentication failed"
        }
        return HttpResponse(json.dumps(res))





@api_view(['POST'])
@permission_classes((''))
def setAlt(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split(' ')[1]

    seller = sellerAuth(userid, token)
    if seller is not None:
        price = request.data['price']
        alt_id = request.data['id']
        stock = request.data['stock']

        try:
            alt_obj = AlternateSeller.objects.get(id= int(alt_id))
            product = getProductById(alt_obj.product_id)
            disc = 100 - ( (int(price) / product.price ) * 100 )

            alt_obj.discount_percentage = disc
            alt_obj.stock = int(stock)
            alt_obj.save()
            res = {
                "success":True,
                "message":"Product info edited successfuly"
            }
            return HttpResponse(json.dumps(res))
        except AlternateSeller.DoesNotExist:
            res = {
                "success":False,
                "message":"couldn't fetch info"
            }
            return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed"
        }
        return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def getCart(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split('Token ')[1]

    customer = custAuth(userid, token)
    if customer is not None:
        cartItems = Cart.objects.filter(customer= customer)
        products = []

        for p in cartItems:
            products.append(p.product)
        
        res = {
            "success":True,
            "cart": serializers.serialize('json', cartItems),
            "products": serializers.serialize('json', products)
        }
        return HttpResponse(json.dumps(res))
    else:
        res = {
            "success":False,
            "message":"Authentication failed"
        }
        return HttpResponse(json.dumps(res))



@api_view(['POST'])
@permission_classes((''))
def removeCart(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split('Token ')[1]

    customer = custAuth(userid, token)
    if customer is not None:
        product  = request.data["product"]
        Cart.objects.filter(customer= customer, product__slug= product).delete()
        res = {
            "success":True,
            "message":"Product removed from cart successfully"
        }
        
    else:
        res = {
            "success":False,
            "message":"Something went wrong, try after some time"
        }
    return HttpResponse(json.dumps(res))




@api_view(['POST'])
@permission_classes((''))
def returnProduct(request):
    userid = request.data['username']
    token = request.META.get('HTTP_AUTHORIZATION', '').split('Token ')[1]

    customer = custAuth(userid, token)
    if customer is not None:
        orderId = request.data['orderid']
        Order.objects.filter(order_id= orderId).update(status= 'Return requested')
        res = {
            "success": True,
            "message": "Return requested successfully"
        }
    else:
        res = {
            "success":False,
            "message":"Something went wrong, try after some time"
        }
    return HttpResponse(json.dumps(res))

    






#Utitlity functions

def custAuth(custId, token):
    try:
        customer = Customer.objects.get(user__username= custId)
        token_obj = Token.objects.get(user= customer.user, key= token)

        min_time = datetime.datetime.now() - datetime.timedelta(5)
        if token_obj.created.date() > min_time.date():
            return customer
        else:
            token_obj.delete()
            raise Customer.DoesNotExist
    except (Customer.DoesNotExist, Token.DoesNotExist):
        return None







def sellerAuth(sellerid, token):
    try:
        seller = Seller.objects.get(user__username= sellerid)
        token_obj = Token.objects.get(user= seller.user, key= token)
        min_time = datetime.datetime.now() - datetime.timedelta(5)

        if token_obj.created.date() > min_time.date():
            return seller
        else:
            token_obj.delete()
            raise Seller.DoesNotExist
    except (Seller.DoesNotExist, Token.DoesNotExist):
        print('error')
        return None



def getProductById(id):
    try:
        prod = Product.objects.get(id= id)
        return prod
    except Product.DoesNotExist:
        return None



def sendSellerOTP(otp, receiver):
    send_mail('OTP from Shopio', 'Your seller registration OTP is '+ otp, 'shopio.project@gmail.com', [receiver], fail_silently= False)

