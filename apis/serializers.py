from .models import Product, Customer, Seller, AlternateSeller
from rest_framework import serializers, exceptions, permissions
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username','first_name')

class SellerSeializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = Seller
        fields = ('user','id')

class AlternateSellerSerializer(serializers.ModelSerializer):
    seller = SellerSeializer()
    class Meta:
        model = AlternateSeller
        depth=1
        fields = ('id','seller','discount_percentage')

class ProductSerializer(serializers.ModelSerializer):

    seller = SellerSeializer()
    alternate_sellers = AlternateSellerSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        depth = 1
        fields = ('id', 'title', 'description', 'category', 'image1','price','discount_percentage','sub_category', 'seller','slug','alternate_sellers', 'status', 'stock')
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        username = data.get("username","")
        password = data.get("password","")


        if username and password:
            user = authenticate(username = username, password = password)
            seller = Customer.objects.filter(user = user).count()
            if user and seller == 1:
                if user.is_active:
                    data["user"] = user
                else:
                    msg = "user is inactive"
                    raise exceptions.ValidationError(msg)
            else:
                msg = "invalid username or password!"
                raise exceptions.ValidationError(msg)
        else:
            msg = "No username or password."
            raise exceptions.ValidationError(msg)

        return data
