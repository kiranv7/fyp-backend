from django.conf.urls import url
from django.urls import path
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator, AllowedHostsOriginValidator

from chat.consumers import *

application = ProtocolTypeRouter({
    # Empty for now (http->django views is added by default)
    'websocket':AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                [
                    url(r"^chat/(?P<username>[\w.@+-]+)/$", ChatConsumer),
                    url(r"^seller-chat/$", SellerChatConsumer),
                    url(r"^seller-chat-socket/$", SellerEndConsumer),
                    url(r"^customer-chat-socket/$", CustomerChat),
                    url(r"^customer-notification/$", CustomerNotification)
                ]
            )
        )
    )
})