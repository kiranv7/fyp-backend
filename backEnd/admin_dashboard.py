from django.shortcuts import HttpResponse, HttpResponseRedirect, render
from apis.models import *
from django.contrib.auth.models import User
from django.db.models import Q, Sum
from django.contrib.auth.decorators import user_passes_test




def dashboard(request):
    if request.user.is_superuser == True:
        new_sellers = Seller.objects.filter(status="pending")
        new_alt_sellers = AlternateSeller.objects.filter(status="pending")
        new_products = Product.objects.filter(status="pending")
        total_products = Product.objects.filter(status="approved")
        rejected_products = Product.objects.filter(status="rejected")
        alternate_sellers = AlternateSeller.objects.filter(status="approved")
        rejected_alt_sellers = AlternateSeller.objects.filter(status="rejected")
        orders_in_progress = Order.objects.all().exclude(order_status="Cancelled").exclude(order_status="Cancelled by seller")
        cancelled_orders = Order.objects.filter(Q(order_status="Cancelled") | Q(order_status="Cancelled by seller"))
        successful_orders = Order.objects.filter(order_status="Delivered", payment_success=True)
        revenue = successful_orders.aggregate(Sum('amount'))

        context = {
            "new_sellers": new_sellers.count(),
            "new_alt_sellers": new_alt_sellers.count(),
            "new_products": new_products.count(),
            "approved_products":total_products.count(),
            "rejected_products": rejected_products.count(),
            "alternate_sellers": alternate_sellers.count(),
            "rejected_alt_sellers": rejected_alt_sellers.count(),
            "orders_in_progress": orders_in_progress.count(),
            "cancelled_orders": cancelled_orders.count(),
            "successful_orders": successful_orders.count(),
            "revenue":revenue['amount__sum']
        }

        return render(request, 'dashboard.html', context)
    else:
        return HttpResponseRedirect("/")



def newSellers(request):
    if request.user.is_superuser:
        sellers = Seller.objects.filter(status= 'pending')
        
        context = {
            "sellers": sellers,
            "count": sellers.count()
        }
        return render(request, "new-seller.html", context)
    else:
        return HttpResponseRedirect("/")
