"""backEnd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from apis import views
from django.conf.urls.static import static
from django.conf import settings
from apis.views import *
from backEnd.admin_dashboard import *

router = routers.DefaultRouter()
router.register('products', views.ProductViewSet)



urlpatterns = [
    path('', admin.site.urls),
    path('admin-dashboard/', dashboard, name="admin-dashboard"),
    path('admin-dashboard/new-sellers/', newSellers, name="new-sellers"),


    path('api/', include(router.urls)),
    path('api/orders/', orders, name="orderview" ),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/login/', LoginView.as_view()),
    path('api/logout/', LogoutView.as_view()),
    path('api/orders/', orders, name= "ordersapi"),
    path('api/checkToken/', checkToken, name= "checktoken"),
    #path('check/',views.check, name="check"),
    path('api/signup/', SignupView.as_view()),
    path('api/seller-login/', sellerLogin, name="sellerLogin"),
    path('api/seller-dashboard/', sellerDashboard, name="seller-dashboard"),
    path('api/add-to-cart/', addToCart, name="addToCart"),
    path('api/seller-register/', sellerRegister, name="seller-register"),
    path('api/verify-seller/', sellerVerify, name="seller-verify"),
    path('api/resend-seller-otp/', resend_sellerOtp, name="resend-seller-otp"),
    path('api/add-product/', addProduct, name="add-product"),
    path('api/delete-product/', deleteProduct, name="deleteProduct"),
    path('api/get-seller-info/', getSeller, name="get-seller-info"),
    path('api/start-chat/', startChat, name="start-chat"),
    path('api/get-customer/', getCustomer, name="get-customer"),
    path('api/update-profile/', updateProfile, name="update-profile"),
    path('api/add-address/', addAddress, name="add-address"),
    path('api/delete-address/', deleteAddress, name="delete-address"),
    path('api/edit-address/', editAddress, name="edit-address"),
    path('api/checkout-data/', checkoutData, name="checkout-data"),
    path('api/pay/', payment, name="payment"),
    path('paytm/response/<int:cust_id>/', response, name="pay-res"),
    path('api/get-order/', getOrder, name="get-order"),
    path('api/cancel-order/', cancelOrder, name="cancel-order"),
    path('api/get-refund-status/', checkRefundStatus, name="refund-status"),
    path('api/feedback/', feedback, name="store-feedback"),
    path('api/edit-product/', editProduct, name="edit-product"),
    path('api/pending-orders/', pendingOrders, name="pending-orders"),
    path('api/set-expected-delivery/', setOrderStatus, name="set-order-status"),
    path('api/banners/', banners, name="banners"),
    path('api/get-earnings/', getSellerEarnings, name="seller-earnings"),
    path('resize/', resize, name='image_resize'),
    path('api/search-product/', searchProduct, name="search-product"),
    path('api/get-reviews/', getReviews, name="get-reviews"),
    path('api/alternate-seller-request/', altSellerRequest, name="seller-request"),
    path('api/get-alt/', altInfo, name="get-alt"),
    path('api/set-alt/', setAlt, name="set-alt"),
    path('api/cart/', getCart, name="get-cart"),
    path('api/remove-cart/', removeCart, name="remove-cart")
]


urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
